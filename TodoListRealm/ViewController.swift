

import UIKit
import RealmSwift

class ToDoListItem: Object {
    @objc dynamic var item: String = ""
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var todoTableView: UITableView!
    
    private let realm = try! Realm()
    private var data = [ToDoListItem]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        data = realm.objects(ToDoListItem.self).map({ $0 })
        todoTableView.delegate = self
        todoTableView.dataSource = self
        //todoTableView.rowHeight = 80
    }
    
    
    @IBAction func addTodo(_ sender: UIButton) {
        let alertController = UIAlertController(title: "New task", message: "Add task", preferredStyle: .alert)
        
        let saveTask = UIAlertAction(title: "Save", style: .default) { action in
            
            let text = alertController.textFields?.first
            
            if let newTask = text?.text {
                self.saveTask(withTitle: newTask)
                self.todoTableView.reloadData()
            }
            
            
        }
        
        alertController.addTextField { _ in}
        
        let cancel = UIAlertAction(title: "Cancel", style: .default) { _ in}
        
        alertController.addAction(saveTask)
        alertController.addAction(cancel)
        
        present(alertController, animated: true, completion: nil)
        
        
    }
    
    
    func saveTask(withTitle title: String) {
        
        
        
        let newItem = ToDoListItem()
        newItem.item = title
        
        realm.beginWrite()
        
        realm.add(newItem)
        try! realm.commitWrite()
        data.insert(newItem, at: 0)
        
        
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "todoCell", for: indexPath) as! TodoCell
        
        cell.todoLabel.text = data[indexPath.row].item
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! TodoCell
        
        if cell.isChecked == false {
            cell.checkmarkImage.image = UIImage(named: "checkmark.png")
            cell.isChecked = true
        }
        else {
            cell.checkmarkImage.image = nil
            cell.isChecked = false
        }
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let deleteItemFromRealm = data[indexPath.row]
        realm.beginWrite()
        realm.delete(deleteItemFromRealm)
        try! realm.commitWrite()
        
        
        if editingStyle == .delete {
            data.remove(at: indexPath.row)
            
            todoTableView.reloadData()
        }
    }
    
}

