//
//  TodoCell.swift
//  TodoListRealm
//
//  Created by Виктория Щербакова on 14.11.2020.
//  Copyright © 2020 Виктория Щербакова. All rights reserved.
//

import UIKit

class TodoCell: UITableViewCell {
    
    @IBOutlet weak var checkmarkImage: UIImageView!
    
    @IBOutlet weak var todoLabel: UILabel!
    
     var isChecked = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
